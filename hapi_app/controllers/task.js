const Task = require('../models/task.js');

module.exports = {
  
  async create(request, h) {
    console.log("inside handler");
    var newTask = await new Task({ title: request.payload.title });
    var result = await newTask.save();
    console.log(result);
    return h.redirect('/tasks');
  },
  
  async findAll(request, h) {
    var tasks = await Task.find();
    console.log(tasks);
    return h.view('tasks', { tasks: tasks });
    
  },
  
  // async findOne(request,h){
  //let task = await Task.findById(request.params.id);
  //console.log(task);
  // return h.view('tasks',{tasks: tasks});
  //},
  
  async deleteOne(request, h) {
    let task = await Task.findByIdAndDelete(request.params.id)
    console.log(task);
  },
  
  async errors(request, h, err) {
    let errorMessage = err.message;
    let task = await Task.find()
    return h.view('tasks', { tasks: task, errorMessage: errorMessage }).takeover();
  }
  
}