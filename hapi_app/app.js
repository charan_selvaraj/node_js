const Hapi = require('@hapi/hapi');
const Path = require('path');

//taskModules
const Task = require('./models/task.js');
const TaskController = require('./controllers/task.js');
const taskRoutes = require('./routes/taskRoute.js');

const mongoose = require('mongoose');
mongoose.connect('mongodb://127.0.0.1:27017/hapidb', { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.connection.on('connected', () => console.log('mogoDB Connected.....'));
mongoose.connection.on('error', (err) => console.log('Connection failed with - ',err));


const init = async () => {
  
  //CONNECTION
  const server = Hapi.Server({
    port: 3000,
    host: "localhost",
    // routes : {
    //   files : {
    //     relativeTo: Path.join(__dirname,'public') 
    //   }
    // }
  });
  
  await server.register(require('@hapi/inert'));
  await server.register(require('@hapi/vision'));
  
  
  //congigure views
  server.views({
    engines:{
      html: require('handlebars')
    },
    relativeTo: __dirname,
    path: './views/templates'
  });
  
  
  //Task route
  //AddNew Task
  server.route(taskRoutes);
  
  
  server.ext('onPreHandler', function (request, h) {
    
    switch(request.route.path) {
      case '/tasks':
      console.log("inside pre handler");
      break;
      case '/home':
      break;
    }
    
   return h.continue;
  });
  
  
  
  
  
  
  
  
  server.route({
    method: 'GET',
    path: '/home/{name}/{age}',
    handler: (request,h) => {
      var data = {name: request.params.name, age: request.params.age};
      return h.view('index',data);
    }
  });
  //STATIC ROUTE
  //STATIC FILE SERVING USING h.file()
  server.route({
    method: 'GET',
    path: '/about',
    handler: (request,h) => {
      return h.file('./views/templates/about.html');
    }
  });
  //STATIC FILE SERVING USING file handler
  server.route({
    method: 'GET',
    path: '/contact',
    handler: {
      file: './views/templates/contact.html'
    }
  });
  //STATIC FILE SERVING USING object form of handler
  // server.route({
  //   method: 'GET',
  //   path: '/{filename}',
  //   handler: {
  //     file: function(request){
  //       console.log(request.params);
  //       return request.params.filename;
  //     }
  //   }
  // });
  //STATIC FILE SERVING USING directory handler
  // server.route({
  //   method: 'GET',
  //   path: '/{file}',
  //   handler:{
  //     directory:{
  //       path: './views/templates',
  //       defaultExtension: 'html',
  //       listing: true
  //     }
  //   }
  // });
  
  //ROUTE
  // server.route({
  //   method: 'GET',
  //   path: '/home/{employee?}',
  //   handler: (request,h) => {
  //     const name = request.query.name;
  //     const age = request.query.age;
  //     console.log(request);
  //     return `Hello ${name} Age ${age}!!!!`
  //   }
  // });
  server.route({
    method: 'GET',
    path: '/home/{user?}',
    handler: (request,h) => {
      if(request.params.user){
        return `hello ${request.params.user}`
      }
      // console.log(request);
      return "Hello World"
      // return h.redirect(`/home/{employee?${request.query}}`)
    }
  });
  
  
  
  await server.start();
  console.log(`Server Running at: ${server.info.uri}`);
};



init();
