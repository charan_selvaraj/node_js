const TaskController = require('../controllers/task.js');
const Joi = require('joi');

module.exports = [
  {
    method: "POST",
    path: "/tasks",
    handler: TaskController.create,
    options:{
      validate:{
        payload:Joi.object({
          title: Joi.string().min(3).max(10)
        }),
        failAction: TaskController.errors
      }
    }
  },
  {
    method: "GET",
    path: "/tasks",
    handler: TaskController.findAll
  },
  // {
    //method: "GET",
    //path: "/task/{id}",
    //handler: TaskController.findOne
  //},
  {
    method: "GET",
    path: "/task/{id}",
    handler: TaskController.deleteOne
  }
]

