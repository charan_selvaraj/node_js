import { model, Schema, Model, Document} from 'mongoose';

// const model = require('mongoose');
// const Schema = require('mongoose');
// const Model = require('mongoose');



interface ITodo extends Document {
  title: string;
}

const TodoSchema: Schema = new Schema({
  title: { type: String, required: true },
});

const Todo: Model<ITodo> = model('User', TodoSchema);
// const mongoose = require('mongoose');
// const Todo = mongoose.model('Todo',{title: String});
export default Todo;