// const mongoose = require('mongoose');
// mongoose.Promise = global.Promise;
// let isConnected;
// module.exports = connectToDatabase = () => {
//   mongoose.connect('mongodb+srv://charan_selvaraj:Aspire@123@cluster0.8niy8.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', 
//   { useNewUrlParser: true, useUnifiedTopology: true });
//   mongoose.connection.on('connected', () => console.log('mogoDB Connected.....'));
//   mongoose.connection.on('error', (err) => console.log('Connection failed with - ',err));
// };

// const mongoose = require('mongoose');
import {mongoose} from 'mongoose';
mongoose.Promise = global.Promise;
let isConnected;

module.exports = connectToDatabase = () => {
	if (isConnected) {
		console.log('=> using existing database connection');
		return Promise.resolve();
	}

	console.log('=> using new database connection');
	return mongoose.connect(process.env.DB, { useNewUrlParser: true, useUnifiedTopology: true} ).then(db => {
		isConnected = db.connections[0].readyState;
	});
};
