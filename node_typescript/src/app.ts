import express = require('express');
import { model, Schema, Model, Document } from 'mongoose';
import * as mongoose from 'mongoose';

//Database
mongoose.connect('mongodb://127.0.0.1:27017/typescript', { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.connection.on('connected', () => console.log('mogoDB Connected.....'));
mongoose.connection.on('error', (err) => console.log('Connection failed with - ', err));

//Interface
interface IUser extends Document {
  email: string;
  firstName: string;
  lastName: string;
}

//Schema
const UserSchema: Schema = new Schema({
  email: { type: String, required: true },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true }
});

const User: Model<IUser> = model('User', UserSchema);


//API
const app : express.Application = express();
const port = 3000;
app.get('/', async(req, res) => {
  res.send('Hello World');
});

app.get('/users', async (req,res) => {
  const users: Array<IUser> = await User.find({ email: 'bill@microsoft.com' });
  res.send(users);
})

app.get('/user',async (req,res)=>{
  const user: IUser = await User.create({
    email: 'bill@microsoft.com',
    firstName: 'Bill',
    lastName: 'Gates'
  });

  res.send('Done');

})
app.listen(port,()=>{console.log(`Running at ${port}`)});