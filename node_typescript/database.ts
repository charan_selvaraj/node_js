import Mongoose from "mongoose";
function database(){
Mongoose.connect('mongodb://127.0.0.1:27017/typescript', { useNewUrlParser: true, useUnifiedTopology: true });
Mongoose.connection.on('connected', () => console.log('mogoDB Connected.....'));
Mongoose.connection.on('error', (err) => console.log('Connection failed with - ',err));
}

export default database;
