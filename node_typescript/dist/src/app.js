"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongoose_1 = require("mongoose");
const mongoose_2 = __importDefault(require("mongoose"));
//Database
() => __awaiter(void 0, void 0, void 0, function* () {
    yield mongoose_2.default.connect('mongodb://127.0.0.1:27017/typescript', { useNewUrlParser: true, useUnifiedTopology: true });
    yield mongoose_2.default.connection.on('connected', () => console.log('mogoDB Connected.....'));
    yield mongoose_2.default.connection.on('error', (err) => console.log('Connection failed with - ', err));
});
//Schema
const UserSchema = new mongoose_1.Schema({
    email: { type: String, required: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true }
});
const User = mongoose_1.model('User', UserSchema);
//API
const app = express_1.default();
const port = 3000;
app.get('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const user = yield User.create({
        email: 'bill@microsoft.com',
        firstName: 'Bill',
        lastName: 'Gates'
    });
    res.send('Done');
    // res.send('Hello World');
}));
app.get('/user', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const user = yield User.create({
        email: 'bill@microsoft.com',
        firstName: 'Bill',
        lastName: 'Gates'
    });
    res.send('Done');
}));
// app.listen(port, (err) => {
//   if (err) {
//     return console.error(err);
//   }
//   return console.log(`server is listening on ${port}`);
// });
app.listen(port, () => { console.log(`Running at ${port}`); });
//# sourceMappingURL=app.js.map