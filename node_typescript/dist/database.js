"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
function database() {
    mongoose_1.default.connect('mongodb://127.0.0.1:27017/typescript', { useNewUrlParser: true, useUnifiedTopology: true });
    mongoose_1.default.connection.on('connected', () => console.log('mogoDB Connected.....'));
    mongoose_1.default.connection.on('error', (err) => console.log('Connection failed with - ', err));
}
exports.default = database;
//# sourceMappingURL=database.js.map