// without callback 
console.log("start");
function login(email, password){
  setTimeout(()=>{
    return {userEmail: email}
  },3000)
}
const user = login("charanselvaraj1998@gmail.com","123456");
console.log(user);
console.log("end");




//with callback
console.log("start");
function login(email, password, callback){
  setTimeout(()=>{
    callback({userEmail: email});
  },3000)
}
function userName(name,callback){
  setTimeout(()=>{
    callback({userName: name});
  },3000)
}
const user = login("charanselvaraj1998@gmail.com","123456",(user)=>{
console.log(user);
userName("charan",(user_name)=>{
  console.log(user_name);
})
});
console.log("end");
//In the above code we are using callback inside another callback and it will go on and it
//will end up with callback hell. to overcome this we are using promises,



//Promise
//sample promise code
console.log("start");
const promise = new Promise((resolve,reject)=>{
  setTimeout(()=>{
    resolve("user logged");
    reject(new Error("User not logged in"));
  },3000)
})
promise.then((user)=>{
  console.log(user);
}).catch((err)=>{
  console.log(err.message)
})
console.log("end");



//Implement above callback example using promise
console.log("start");
function login(email, password){
  return new Promise((resolve,reject)=>{
    setTimeout(()=>{
      resolve({userEmail: email});
    },3000)
  })
}
function userName(name){
  return new Promise((resolve,reject)=>{
    setTimeout(()=>{
      resolve({userName: name});
    },3000)
  })
}
login("charanselvaraj1998@gmail.com","123456").then((user)=>{
  console.log(user);
}).then(()=>{
  userName("charan").then((user_name)=>{
    console.log(user_name)
  })
})
console.log("end");




//To excecute more promises at same time
const youtube = new Promise((resolve,reject)=>{
  console.log("hello from youtube");
  setTimeout(()=>{
    resolve({video: [1,2,3,4,5]})
  },2000)
})
const facebook = new Promise((resolve,reject)=>{
  console.log("hello from facebook");
  setTimeout(()=>{
    resolve({user: "charan"})
  },5000)
})
Promise.all([youtube,facebook]).then((result)=>{console.log(result)});



//Using async() and await
console.log("start");
function login(email, password){
  return new Promise((resolve,reject)=>{
    setTimeout(()=>{
      resolve({userEmail: email});
    },3000)
  })
}
function userName(name){
  return new Promise((resolve,reject)=>{
    setTimeout(()=>{
      resolve({userName: name});
    },3000)
  })
}
async function displayDetails(){
  const user_mail = await login("charan.selvaraj@aspiresys.com","password");
  console.log(user_mail);
  const user_name = await login("kavin kumar");
  console.log(user_name);
}
displayDetails();
console.log("end");




