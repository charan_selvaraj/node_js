var now = require("performance-now");


//sync
var start = now()
const array = [1,2,3,4,5]
array.forEach(element => {
  console.log(element);
});
var end = now()
console.log((end-start).toFixed(3)) 




//async
var startAsync = now();
function asyncForEach(array,callback){
  array.forEach((i)=>{
    setTimeout(() => callback(i), 0)
  })
}
const arrayAsync = [1,2,3,4,5];
asyncForEach(arrayAsync,(i)=>{
  console.log(i);
})
var endAsync = now();
console.log((endAsync-startAsync).toFixed(3)) 

