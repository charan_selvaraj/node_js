const events = require('events');
// const myEmitter = new events.EventEmitter();
// myEmitter.on('messageLog',(name)=>{
//   console.log("hi" + name);
// })
// myEmitter.emit('messageLog','charan');
const util = require('util');
var Person = function(name){
  this.name = name;
} 

util.inherits(Person, events.EventEmitter);

var charan = new Person("charan");
var swathi = new Person("swathi");
var kavin = new Person("kavin");

var people = [charan,swathi,kavin];
people.forEach((person)=>{
  person.on('speak',(msg)=>{
    console.log(person.name +" "+ "said :" + msg);
  });
});
charan.emit('speak','hello doods');
swathi.emit('speak','hello doods');
kavin.emit('speak','hello doods');

