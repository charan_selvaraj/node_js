// Normal Function Statement
function sayHi(){
  console.log("hi");
}
sayHi();

// Function Expression - Assigning Anonymous function to a variable is function Expression
var sayBye = function(){
  console.log("bye");
}
sayBye();

// Passing function to another function
function callFunction(fun){
  fun();
}

function display(){
  console.log("hello charan");
}

callFunction(display);

