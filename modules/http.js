var http = require('http');
var fs = require ('fs');
var server = http.createServer((req,res)=>{
  if(req.url==='/'){
    res.writeHead(200,{'Content-Type' : 'application/json'})
    // var myReadStream = fs.createReadStream(__dirname + '/demo1.txt','utf8');
    // myReadStream.pipe(res);
    var employee = {
      name: "charan",
      age: 22,
      department: "development"
    }
    res.end(JSON.stringify(employee));
  }
});

server.listen(3000);