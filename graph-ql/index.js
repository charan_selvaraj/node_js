import express from 'express';
import resolvers from './resolvers';
import schema from './schema';
import { graphqlHTTP } from 'express-graphql';
// const express = require('express');
var app = express();
app.get("/",(req,res)=>{
    res.send("Up Running Express App");
});

const root = resolvers;

app.use('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true
}))

app.listen(3000,()=>{console.log("Running at 3000")});